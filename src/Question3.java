
public class Question3 {

	public static void main(String[] args) {
		char[] arr = {'b', '!', '2', 'Z'};
		int len = arr.length; //4
		char[] copy = new char[len];
		
		for(int i = 0; i < len; i++) { //int = 3
			char current = arr[i]; //current = Z
			int copyPosition = len - 1 - i; //copyPos = 0
			copy[copyPosition] =  current; // {Z ,2 ,!, b}
		}
		
		System.out.println(copy);
		System.out.print("{");
		for(int i = 0; i < len; i++) {
			System.out.print("'" + copy[i] + "'");
			if(i < len - i) {
				System.out.print(",");
			}
		}
		System.out.print("}");
		
		//solution = {'Z', '2', '!', 'b'};

	}

}
