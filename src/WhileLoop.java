import java.util.Scanner;

public class WhileLoop {
	public static void main(String args[]) {
		copyArray();
//		int size = 200000;
//		int[] arr = new int[size];
//		
//		for(int i = 0; i < arr.length; i++) {
//			arr[i] = i + 1;
//		}
//		
//		for(int j = arr.length - 1; j >= 0 ; j--) {
//			System.out.println(arr[j]);
//		}		
	}
	
	public static void copyArray() {
		int[] arr1 = {6,7,8,9,0};
		int[] arr2 = {1,2,3,4,5};
		arr1 = arr2;
		arr2[0] = 9;
		
		for(int i = 0; i < arr1.length; i++) {
			System.out.println("arr1["+i+"] = " + arr1[i]);
			System.out.println("arr2["+i+"] = " + arr2[i]);
		}	
	}
}
