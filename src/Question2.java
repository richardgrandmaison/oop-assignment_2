
public class Question2 {

	public static void main(String[] args) {
		int[] arr = {11, -7, 34, 0, 4, -23};
		
		int min = arr[0];
		int max = arr[0]; 
		int sum = 0;
		
		for(int i = 0; i < arr.length; i++) {
			int current = arr[i]; 
			if (current < min) {
				min = current;
			}
			if (current > max) {
				max = current;
			}
			sum += current;
		}
		
		System.out.println("Min = " + min);
		System.out.println("Max = " + max);
		System.out.println("Avg = " + (double) sum / arr.length);
	}

}
